import { SET_LANGUAGE } from "./types"
import { AsyncStorage } from "react-native"

// This will set language in redux
export const setLanguage = lang => async dispatch => {
  dispatch({
    type: SET_LANGUAGE,
    payload: lang
  })
}
