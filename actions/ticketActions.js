import axios from "axios"
import { SET_TICKETS, ADD_TICKET } from "./types"
import url from "../services/config"
import { ToastAndroid } from "react-native"
// This file contains all redux actions that have anything to do with user's tickets state in the app

export const sendTicket = ticket => async dispatch => {
  const route = "/tickets/add"

  try {
    const response = await axios.post(`${url}${route}`, ticket)

    dispatch({
      type: ADD_TICKET,
      payload: response.data
    })
    ToastAndroid.show("Maksu suoritettu onnistuneesti!", ToastAndroid.SHORT)
    return response.data
  } catch (err) {
    console.log(err)
    // Display toast
    ToastAndroid.show(
      "Virhe suoritettaessa maksua, yritä myöhemmin uudelleen",
      ToastAndroid.SHORT
    )
    return null
  }
}

// This retrieves all tickets for the logged in user and updates app state (redux)
export const retrieveUserTickets = () => async dispatch => {
  const route = "/tickets/retrieve"

  try {
    const response = await axios.get(`${url}${route}`)
    console.log(response.data)

    dispatch({
      type: SET_TICKETS,
      payload: response.data
    })
    return response.data
  } catch (err) {
    console.log("Error at retrieveUserTickets", err)
  }
}
