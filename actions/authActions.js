import { SET_CURRENT_USER, CLEAR_USER } from "./types"
import { AsyncStorage } from "react-native"
import setAuthToken from "../utils/setAuthToken"
import url from "../services/config"
import { ToastAndroid } from "react-native"
import axios from "axios"
// This file contains all redux actions that have anything to do with user authentication state in the app

export const registerNewUser = (email, password) => async dispatch => {
  const route = "/users/register"
  let token

  try {
    const response = await axios.post(`${url}${route}`, { email, password })
    token = response.data.token
    const roleId = response.data.roleId
    // console.log(response.data)
    // When user registers, token is added to authorization header, AsyncStorage and redux
    const userForAsyncStorage = {
      jwtToken: token,
      email: email,
      roleId: roleId
    }
    // This async call sets the stringified object into devices persistent memory
    // so that the information persists between app start/shutdowns. This keeps the user logged in.
    await AsyncStorage.setItem("user", JSON.stringify(userForAsyncStorage))
    // This configures axios to include auth token in all requests headers ("../utils/setAuthToken")
    setAuthToken(token)
    dispatch({
      type: SET_CURRENT_USER,
      payload: { token, email, roleId }
    })
  } catch (err) {
    console.log("ON CATCH")
    console.log(err.response.data)
    err.response.data.errors.forEach((error, index) => {
      // Display toast
      ToastAndroid.showWithGravityAndOffset(
        error.msg,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        0,
        index * 25
      )
    })
  }
}

export const loginUser = (email, password) => async dispatch => {
  const route = "/users/login"
  let token

  try {
    const response = await axios.post(`${url}${route}`, { email, password })
    token = response.data.token
    const roleId = response.data.roleId
    const userForAsyncStorage = {
      jwtToken: token,
      email: email,
      roleId: roleId
    }
    // This async call sets the stringified object into devices persistent memory
    // so that the information persists between app start/shutdowns. This keeps the user logged in.
    await AsyncStorage.setItem("user", JSON.stringify(userForAsyncStorage))
    // This configures axios to include auth token in all requests headers ("../utils/setAuthToken")
    setAuthToken(token)
    dispatch({
      type: SET_CURRENT_USER,
      payload: { token, email, roleId }
    })
  } catch (err) {
    console.log(err)
  }
}

export const setUserLoggedIn = (token, email, roleId) => async dispatch => {
  console.log("Loggin in user: ", token, email, roleId)
  setAuthToken(token)
  dispatch({
    type: SET_CURRENT_USER,
    payload: { token, email, roleId }
  })
}

export const logOut = () => async dispatch => {
  console.log("onLogout..")
  try {
    // Delete user from AsyncStorage (Permanent storage)
    await AsyncStorage.removeItem("user")
    // Delete axios headers
    setAuthToken(false)
    // Remove user from redux-store (App runtime-storage)
    dispatch({
      type: CLEAR_USER
    })
  } catch (err) {
    console.log("Logout failed", err)
  }
}

// Changes users role (guest, student, staff) in app, called from the dropdown menu in settingscreen
export const changeUserRole = (user, roleId, toastMsg) => async dispatch => {
  const route = "/users/updaterole"
  try {
    const newRoleId = roleId
    const response = await axios.post(`${url}${route}`, { newRoleId })

    const userForAsyncStorage = {
      jwtToken: user.token,
      email: user.email,
      roleId: newRoleId
    }

    await AsyncStorage.setItem("user", JSON.stringify(userForAsyncStorage))

    ToastAndroid.show(toastMsg, ToastAndroid.SHORT)

    dispatch({
      type: SET_CURRENT_USER,
      payload: { token: user.token, email: user.email, roleId: newRoleId }
    })
  } catch (err) {
    ToastAndroid.show("Server error, try again later", ToastAndroid.SHORT)
    console.log(err)
  }
}
