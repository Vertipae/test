import React from "react"
import { Platform } from "react-native"
import {
  createStackNavigator,
  createBottomTabNavigator,
  getActiveChildNavigationOptions
} from "react-navigation"

import TabBarIcon from "../components/TabBarIcon"
import HomeScreen from "../screens/HomeScreen"
import TicketsScreen from "../screens/TicketsScreen"
import ProductsScreen from "../screens/ProductsScreen"
import SettingsScreen from "../screens/SettingsScreen"
// In this file, a StackNavigator is defined for each separate tab (tabs at the bottom of the screen)
// https://reactnavigation.org/docs/en/stack-navigator.html
const config = Platform.select({
  web: { headerMode: "screen" },
  default: {}
})

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen
  },
  config
)

HomeStack.navigationOptions = ({ navigation, screenProps }) => {
  const parentProps = getActiveChildNavigationOptions(navigation, screenProps)
  return {
    tabBarLabel: parentProps.title,
    tabBarIcon: ({ focused }) => (
      <TabBarIcon
        focused={focused}
        name={
          Platform.OS === "ios"
            ? `ios-restaurant${focused ? "" : "-outline"}`
            : "md-restaurant"
        }
      />
    )
  }
}

HomeStack.path = ""

const TicketsStack = createStackNavigator(
  {
    Tickets: TicketsScreen
  },
  config
)

TicketsStack.navigationOptions = ({ navigation, screenProps }) => {
  const parentProps = getActiveChildNavigationOptions(navigation, screenProps)
  return {
    tabBarLabel: parentProps.title,
    tabBarIcon: ({ focused }) => (
      <TabBarIcon
        focused={focused}
        name={
          Platform.OS === "ios"
            ? `ios-cash${focused ? "" : "-outline"}`
            : "md-cash"
        }
      />
    )
  }
}

TicketsStack.path = ""

const ProductsStack = createStackNavigator(
  {
    Links: ProductsScreen
  },
  config
)

ProductsStack.navigationOptions = ({ navigation, screenProps }) => {
  const parentProps = getActiveChildNavigationOptions(navigation, screenProps)
  return {
    tabBarLabel: parentProps.title,
    tabBarIcon: ({ focused }) => (
      <TabBarIcon
        focused={focused}
        name={Platform.OS === "ios" ? "ios-cart" : "md-cart"}
      />
    )
  }
}

ProductsStack.path = ""

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen
  },
  config
)

SettingsStack.navigationOptions = ({ navigation, screenProps }) => {
  const parentProps = getActiveChildNavigationOptions(navigation, screenProps)
  return {
    tabBarLabel: parentProps.title,
    tabBarIcon: ({ focused }) => (
      <TabBarIcon
        focused={focused}
        name={Platform.OS === "ios" ? "ios-person" : "md-person"}
      />
    )
  }
}

SettingsStack.path = ""

const tabNavigator = createBottomTabNavigator(
  {
    HomeStack,
    ProductsStack,
    TicketsStack,
    SettingsStack
  },
  {
    lazy: false
  }
)

tabNavigator.path = ""

export default tabNavigator
