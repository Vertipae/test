import axios from "axios"
import url from "./config"

// This is called from ProductScreen.js when the application starts
// Will retrieve all available products from the store (lunch, coffee..)
export const fetchAllProducts = async id_role => {
  const route = "/products"
  const testId = id_role
  const response = await axios.get(`${url}${route}/${testId}`)
  return response.data
}
