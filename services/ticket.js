import axios from "axios"
import url from "./config"
// Moved to redux ticketActions.js
// export const sendTicket = async ticket => {
//   const route = '/tickets/add'
//   // const ticket = { a: 'aa', b: 'bb' }

//   try {
//     const response = await axios.post(`${url}${route}`, ticket)
//     console.log(response.data)
//     return response.data
//   } catch (err) {
//     console.log(err)
//     return null
//   }
//   //console.log(response.data)
// }

// export const retrieveUserTickets = async ticket => {
//   const route = '/tickets/retrieve'

//   try {
//     const response = await axios.get(`${url}${route}`)
//     console.log(response.data)
//     return response.data
//   } catch (err) {
//     console.log('Error at retrieveUserTickets', err)
//   }
// }

// Development code/feature only, will not be needed in the production version
export const useTicket = async ticket => {
  const route = "/tickets/use"

  try {
    const ticketJson = JSON.parse(ticket)
    const response = await axios.post(`${url}${route}`, ticketJson)
    console.log("Ticket has been used succesfully!")
  } catch (err) {
    console.log("Error at using ticket")
    console.log(err)
  }
}
