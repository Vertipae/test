// This code fetches the menu from Jamix API and constructs it properly into an array for an ease of use in MenuScreen.js
const url =
  "http://www.jamix.fi/ruokalistapalvelu/rest/haku/menu/97090/1?lang=fi"
const weekDays = [
  "Sunnuntai",
  "Maanantai",
  "Tiistai",
  "Keskiviikko",
  "Torstai",
  "Perjantai",
  "Lauantai"
]

// The result from Jamix API is a bit of a mess, we are using this function
// to find the specific object inside that response, it's named 'days', in the result
const findVal = (object, key) => {
  var value
  Object.keys(object).some(function(k) {
    if (k === key) {
      value = object[k]
      return true
    }
    if (object[k] && typeof object[k] === "object") {
      value = findVal(object[k], key)
      return value !== undefined
    }
  })
  return value
}

// This is the date string that is displayed in the foodmenu list
const getDateString = (date, daynr) => {
  const dd = date.slice(6, 8)
  const mm = date.slice(4, 6)
  const dayName = weekDays[daynr]
  return `${dayName} (${dd}-${mm})`
}

// Format the data into more fitting form, an array of objects and sort by date (each object will be one day)
const formatData = data => {
  data = data.sort((a, b) => b.date < a.date)
  let resModified = []

  data.forEach(day => {
    resModified = resModified.concat({
      dateString: getDateString(day.date.toString(), day.weekday),
      ...day
    })
  })
  return resModified
}

export const fetchMenu = async () => {
  let res = await fetch(url).then(res => res.json())
  //res = await res.json()

  if (res.length === 0) {
    return false
  }

  const days = findVal(res, "days")
  const resultFormatted = formatData(days)
  return resultFormatted
}
