// Language mappings, components import this and select the correct language
// based on state in redux, there is a lang value which is always either 'en' or 'fi'
export default {
  MENU_NOT_AVAILABLE: {
    en: "Sorry, but the menu is not available at the moment",
    fi: "Valitettavasti ruokalista ei ole tällä hetkellä käytettävissä"
  },
  OPEN_INGREDIENTS_DETAILS: {
    en: "Tap a any meal to open list of ingredients",
    fi: "Ruoka-annosta painamalla avautuu ainesosaluettelo"
  },
  LOGIN_TO_PURCHASE: {
    en: "Please login to buy tickets",
    fi: "Kirjaudu sisään ostaaksesi lipukkeita"
  },
  BUY: {
    en: "Buy",
    fi: "Osta"
  },
  PLACEHOLDER_EMAIL: {
    en: "Email",
    fi: "Sähköposti"
  },
  PLACEHOLDER_PASSWORD: {
    en: "Password",
    fi: "Salasana"
  },
  TITLE_REGISTER: {
    en: "Register",
    fi: "Rekisteröidy"
  },
  TITLE_LOGIN: {
    en: "Login",
    fi: "Kirjaudu"
  },
  AGREEMENT: {
    en: "By using this application you agree to our ",
    fi: "Käyttämällä sovellusta hyväksyt "
  },
  TERMS_OF_USE: {
    en: "terms of use ",
    fi: "yleiset käyttöehdot "
  },
  AND: {
    en: "and ",
    fi: "ja "
  },
  OR: {
    en: "or",
    fi: "tai"
  },
  PRIVACY_POLICY: {
    en: "our privacy policy",
    fi: "tietosuojaselosteen"
  },
  OPEN_QRCODE_BY_TAPPING: {
    en: "Open the QR-code by tapping",
    fi: "Avaa QR-koodi painamalla lipuketta"
  },
  CLOSE: {
    en: "Close",
    fi: "Sulje"
  },
  INGREDIENTS: {
    en: "Ingredients",
    fi: "Ainesosat"
  },
  AUTH_AS_STUDENT_SUCCESS: {
    en: "Successfully authenticated as a student!",
    fi: "Opiskelijaksi tunnistautuminen onnistui"
  },
  AUTH_AS_PERSONNEL_SUCCESS: {
    en: "Successfully authenticated as a school's personnel!",
    fi: "Henkilökunnan jäseneksi tunnistautuminen onnistui!"
  },
  LOGOUT: {
    en: "Logout",
    fi: "Kirjaudu ulos"
  },
  GUEST: {
    en: "Guest",
    fi: "Vieras"
  },
  STUDENT: {
    en: "Student",
    fi: "Opiskelija"
  },
  PERSONNEL: {
    en: "Personnel",
    fi: "Henkilökunta"
  },
  MENU: {
    en: "Menu",
    fi: "Ruokalista"
  },
  PRODUCTS: {
    en: "Products",
    fi: "Tuotteet"
  },
  TICKETS: {
    en: "Tickets",
    fi: "Lipukkeet"
  },
  PROFILE: {
    en: "Profile",
    fi: "Profiili"
  },
  WELCOME: {
    en: "Welcome,",
    fi: "Tervetuloa,"
  },
  AUTHENTICATE_AS: {
    en: "Authenticate as",
    fi: "Tunnistaudu"
  },
  CHOOSE_LANGUAGE: {
    en: "Choose language",
    fi: "Valitse kieli"
  }
}
