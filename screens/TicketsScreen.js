import React, { useState, useEffect } from "react"
import {
  Alert,
  ScrollView,
  StyleSheet,
  Text,
  Modal,
  TouchableWithoutFeedback,
  View
} from "react-native"
import { ListItem, Button } from "react-native-elements"
import QRCode from "react-native-qrcode"
const uuidv4 = require("uuid/v4")
import { connect } from "react-redux"
import { retrieveUserTickets } from "../actions/ticketActions"
import { useTicket } from "../services/ticket"
import LANG from "../constants/Lang"

const TicketsScreen = props => {
  const [ticketModal, setTicketModal] = useState({ open: false })

  // On first render or when user changes app retrieves tickets from the database
  useEffect(() => {
    if (props.auth.isAuthenticated) {
      props.retrieveUserTickets()
    }
  }, [props.auth.user.email, props.auth.user.roleId])

  // Change navigation language
  useEffect(() => {
    props.navigation.setParams({ title: LANG.TICKETS[props.lang] })
  }, [props.lang])

  // Opens/closes ticket modal
  const toggleModal = state => {
    const ticketValue = JSON.stringify(state.ticketValue)
    setTicketModal({ open: state.open, ticketValue: ticketValue })
  }
  // This is to prevent mapping problems with tickets when rendering before fetching
  if (!props.tickets.tickets) {
    return null
  }
  // If not authenticated return text indicating that
  if (!props.auth.isAuthenticated) {
    return (
      <View style={styles.container}>
        <Text>{LANG.LOGIN_TO_PURCHASE[props.lang]}</Text>
      </View>
    )
  } else {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollStyle}>
          <Text style={{ fontSize: 12, lineHeight: 12, marginBottom: 5 }}>
            {LANG.OPEN_QRCODE_BY_TAPPING[props.lang]}
          </Text>
          {props.tickets.tickets.map(ticket => (
            <ListItem
              onPress={() =>
                toggleModal({
                  open: true,
                  ticketValue: {
                    //id: ticket.id,
                    id: ticket.order_id
                    //transaction_id: ticket.transaction_id
                  }
                })
              }
              key={uuidv4()}
              title={ticket.name}
              titleStyle={{ fontWeight: "bold" }}
              subtitle={`${(ticket.amount / 100).toFixed(2)}€`}
              containerStyle={{
                borderBottomWidth: 1,
                borderBottomColor: "rgba(242, 135, 183, 0.3)",
                marginRight: 20
              }}
            />
          ))}
        </ScrollView>
        <Modal
          visible={ticketModal.open}
          transparent={true}
          animationType='fade'
          // for Android // The onRequestClose callback is called when the user taps the hardware back button on Android or the menu button on Apple TV
          onRequestClose={() => toggleModal({ open: false, ticketValue: {} })}
        >
          <TouchableWithoutFeedback
            onPress={() => toggleModal({ open: false, ticketValue: {} })}
          >
            <View style={styles.touchable}>
              <QRCode size={300} value={ticketModal.ticketValue} />
              <Button
                containerStyle={styles.buttonContainerStyle}
                buttonStyle={styles.buttonStyle}
                title='Sulje'
                onPress={() => toggleModal({ open: false, ticketValue: {} })}
              />
              {/* This is only used for testing if no QR-code reader is available */}
              <Button
                containerStyle={styles.buttonContainerStyle}
                buttonStyle={styles.buttonStyle}
                title='Lunasta'
                onPress={() => {
                  console.log(ticketModal.ticketValue)
                  useTicket(ticketModal.ticketValue)
                }}
              />
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    )
  }
}

TicketsScreen.navigationOptions = ({ navigation }) => {
  return {
    title: navigation.getParam("title", "Lipukkeet"),
    headerStyle: {
      backgroundColor: "#019dd5"
    },
    headerTitleStyle: {
      color: "#fff"
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff"
  },
  // This is to enable the possibility of closing modal anywhere on the screen
  touchable: {
    height: "100%",
    width: "100%",
    position: "absolute",
    top: 0,
    left: 0,
    backgroundColor: "rgba(255,255,255,0.5)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonStyle: {
    backgroundColor: "#019dd5"
  },
  scrollStyle: {
    marginLeft: 20
  },
  buttonContainerStyle: {
    marginTop: 10
  }
})

const mapStateToProps = state => ({
  auth: state.auth,
  tickets: state.tickets,
  lang: state.settings.lang
})

export default connect(mapStateToProps, { retrieveUserTickets })(TicketsScreen)
