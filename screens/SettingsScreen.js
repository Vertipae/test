import React, { useEffect } from "react"
import { View, StyleSheet } from "react-native"
// import { ExpoConfigView } from '@expo/samples'
import RegisterForm from "../components/RegisterForm"
import UserLoggedIn from "../components/UserLoggedIn"
import { connect } from "react-redux"
import LANG from "../constants/Lang"

const SettingsScreen = function(props) {
  console.log(props.auth.isAuthenticated)
  console.log("NAVIGATION OPTIONS", props.navigation)

  // Change navigation language
  useEffect(() => {
    console.log("LANG CHANGED", props.lang)
    props.navigation.setParams({ title: LANG.PROFILE[props.lang] })
  }, [props.lang])

  if (!props.auth.isAuthenticated) {
    // Render registerform if user is not authenticated
    return (
      <View style={styles.container}>
        <RegisterForm />
      </View>
    )
  } else {
    // Else shows UserLoggedIn component
    return (
      <View style={styles.container}>
        <UserLoggedIn />
      </View>
    )
  }
}

SettingsScreen.navigationOptions = ({ navigation }) => {
  return {
    title: navigation.getParam("title", "Profiili"),
    headerStyle: {
      backgroundColor: "#019dd5"
    },
    headerTitleStyle: {
      color: "#fff"
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

const mapStateToProps = state => ({
  auth: state.auth,
  lang: state.settings.lang
})

export default connect(mapStateToProps)(SettingsScreen)
