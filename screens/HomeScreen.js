import * as WebBrowser from "expo-web-browser"
import React, { useState, useEffect } from "react"
import { ScrollView, StyleSheet, Text, View, AsyncStorage } from "react-native"

import { fetchMenu } from "../services/menu"
import { setUserLoggedIn } from "../actions/authActions"
import { connect } from "react-redux"
import LANG from "../constants/Lang"

const uuidv4 = require("uuid/v4")
import { ModalMeal } from "../components/ModalMeal"

const HomeScreen = props => {
  const [menu, setMenu] = useState([])
  const [modal, setModal] = useState({ open: false, mealdetails: "" })

  // This is called only once, when the application starts. It will retrieve user
  // from devices memory, AsyncStorage
  useEffect(() => {
    getInitialState()
  }, [])

  // This is called whenever lang changes in redux
  useEffect(() => {
    // Set title text
    props.navigation.setParams({ title: LANG.MENU[props.lang] })
  }, [props.lang])

  const getInitialState = async () => {
    let user = await AsyncStorage.getItem("user")

    if (user) {
      user = JSON.parse(user)
      // dispatch...
      props.setUserLoggedIn(user.jwtToken, user.email, user.roleId)
    } else {
      console.log("No user found, not logged in")
    }

    // Will download the food menu from Jamix API
    const menu = await fetchMenu()
    setMenu(menu)
  }

  const renderMenu = () => {
    return (
      <ScrollView style={styles.scrollStyle}>
        {menu.map(day => (
          <View key={uuidv4()}>
            <Text style={styles.headerStyle}>{day.dateString}</Text>

            {day.mealoptions.map(meal => (
              <View key={uuidv4()}>
                <Text style={styles.mealStyle}>{`${meal.name}`}</Text>
                <Text
                  style={{ marginLeft: 15, lineHeight: 26 }}
                  onPress={() =>
                    setModal({
                      open: true,
                      mealdetails: meal.menuItems[0].ingredients
                    })
                  }
                >
                  {`${meal.menuItems[0].name} (${meal.menuItems[0].diets})`}
                </Text>
              </View>
            ))}
          </View>
        ))}
      </ScrollView>
    )
  }

  const renderMenuNotAvailable = () => {
    return <Text>{LANG.MENU_NOT_AVAILABLE[props.lang]}</Text>
  }

  return (
    <View
      style={{
        marginLeft: 20,
        marginTop: 20
      }}
    >
      <Text style={{ fontSize: 12, lineHeight: 12, marginBottom: 5 }}>
        {LANG.OPEN_INGREDIENTS_DETAILS[props.lang]}
      </Text>
      {menu !== false ? renderMenu() : renderMenuNotAvailable()}

      <ModalMeal
        visible={modal.open}
        mealdetails={modal.mealdetails}
        close={() => setModal({ open: false })}
      />
    </View>
  )
}

HomeScreen.navigationOptions = ({ navigation }) => {
  return {
    title: navigation.getParam("title", "Ruokalista"),
    headerStyle: {
      backgroundColor: "#019dd5"
    },
    headerTitleStyle: {
      color: "#fff"
    }
  }
}

const styles = StyleSheet.create({
  headerStyle: {
    marginTop: 20,
    fontWeight: "bold",
    fontSize: 22,
    color: "#019dd5"
  },
  scrollStyle: {
    marginBottom: 20
  },
  cardStyle: {
    flex: 1
  },
  mealStyle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "#f287b7"
  },
  titleStyle: {
    fontSize: 20
  }
})

const mapStateToProps = state => ({
  lang: state.settings.lang
})

export default connect(mapStateToProps, { setUserLoggedIn })(HomeScreen)
