import React, { useState, useEffect } from "react"
import { ScrollView, StyleSheet, Text, View, Platform } from "react-native"
import { ListItem, Button } from "react-native-elements"
const uuidv4 = require("uuid/v4")
import { connect } from "react-redux"
import { Ionicons } from "@expo/vector-icons"
import { fetchAllProducts } from "../services/product"
import { sendTicket } from "../actions/ticketActions"
import Spinner from "../components/Spinner"
import LANG from "../constants/Lang"

const ProductsScreen = props => {
  const [products, setProducts] = useState([])
  console.log("Rendering productsScreen.", props.auth.user)

  // This is called on first render and when user's email or role changes
  useEffect(() => {
    // Fetches correct products and prices from the database
    fetchProducts()
  }, [props.auth.user.email, props.auth.user.roleId])

  // Change navigation language
  useEffect(() => {
    props.navigation.setParams({ title: LANG.PRODUCTS[props.lang] })
  }, [props.lang])

  // Fetches correct products and prices from the database
  const fetchProducts = async () => {
    console.log("USER:", props.auth.user)
    // If user is not authenticated, set roleId to 1 (guest)
    const roleId = props.auth.isAuthenticated ? props.auth.user.roleId : 1
    const products = await fetchAllProducts(roleId)
    console.log(products)
    setProducts(products)
  }

  // Fake MobilePay payment, the real MobilePay-app returns 2 ids
  const fakeMobilePayPayment = async product_id => {
    // Make MobilePay payment here and give it the amount, MobilePay returns with transaction and order id's
    // ...const { transaction_id, order_id } = await MobilePay.makePayment(amount)
    const transaction_id = getRandomId()
    const order_id = getRandomId()

    const ticket = {
      transaction_id,
      order_id,
      product_id: product_id
    }

    console.log(ticket)
    props.sendTicket(ticket)
  }
  // Generating randomId for transaction_id & order_id
  const getRandomId = () => {
    const chars = [..."0123456789"]
    const randomId = [...Array(10)].map(
      i => chars[(Math.random() * chars.length) | 0]
    ).join``
    return randomId
  }
  if (products.length <= 0) {
    return <Spinner />
  }

  return (
    <View style={styles.container}>
      <ScrollView style={styles.scrollStyle}>
        {props.auth.isAuthenticated ? null : (
          <Text style={{ fontSize: 12, lineHeight: 12, marginBottom: 5 }}>
            {LANG.LOGIN_TO_PURCHASE[props.lang]}
          </Text>
        )}
        {products.map(product => (
          <ListItem
            onPress={() => console.log("Item pressed")}
            key={uuidv4()}
            title={product.name}
            titleStyle={{ fontWeight: "bold" }}
            subtitle={`${(product.amount / 100).toFixed(2)}€`}
            // bottomDivider
            rightElement={
              <Button
                disabled={props.auth.isAuthenticated ? false : true}
                onPress={() => fakeMobilePayPayment(product.id)}
                title={LANG.BUY[props.lang]}
                buttonStyle={styles.buttonStyle}
                icon={
                  <Ionicons
                    name={
                      Platform.OS === "ios"
                        ? `ios-cart${focused ? "" : "-outline"}`
                        : "md-cart"
                    }
                    size={24}
                    color='white'
                    style={{ marginRight: 2 }}
                  />
                }
              />
            }
            containerStyle={{
              borderBottomWidth: 1,
              borderBottomColor: "rgba(242, 135, 183, 0.3)"
            }}
          />
        ))}
      </ScrollView>
    </View>
  )
}

ProductsScreen.navigationOptions = ({ navigation }) => {
  return {
    title: navigation.getParam("title", "Tuotteet"),
    headerStyle: {
      backgroundColor: "#019dd5"
    },
    headerTitleStyle: {
      color: "#fff"
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff",
    marginHorizontal: 20
  },
  buttonStyle: {
    backgroundColor: "#019dd5"
  }
})

const mapStateToProps = state => ({
  auth: state.auth,
  lang: state.settings.lang
})

export default connect(mapStateToProps, { sendTicket })(ProductsScreen)
