import React, { Component } from "react"
import { Text, View, Image } from "react-native"
import spinner from "./spinner.gif"
// This is the spinner that is shown on loading
export class Spinner extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image source={spinner} style={{ width: 200 }} alt='Loading..'></Image>
      </View>
    )
  }
}

export default Spinner
