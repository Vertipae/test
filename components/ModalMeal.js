import React, { Component } from "react"
import {
  Text,
  View,
  Modal,
  Alert,
  StyleSheet,
  TouchableWithoutFeedback
} from "react-native"
// This returns the modal component

// https://github.com/facebook/react-native/issues/17986 Modal fix seurata
const ModalMeal = ({ visible, mealdetails, close }) => {
  console.log("visible:", visible)
  return (
    <Modal
      visible={visible}
      transparent={true}
      // presentationStyle={"overFullScreen"}
      animationType='fade'
      // For Android // The onRequestClose callback is called when the user taps the hardware back button on Android or the menu button on Apple TV
      onRequestClose={() => {
        Alert.alert("Modal has been closed. Test")
      }}
    >
      <TouchableWithoutFeedback onPress={() => close()}>
        <View
          style={{
            height: "100%",
            width: "100%",
            position: "absolute",
            top: 0,
            left: 0,
            backgroundColor: "rgba(255,255,255,0.5)",
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View
            style={styles.containerStyle}
            onPress={() => console.log("pressing works")}
          >
            <Text style={{ fontWeight: "bold", fontSize: 16 }}>Ainesosat</Text>
            <Text>{mealdetails}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: "white",
    borderWidth: 3,
    borderColor: "#019dd5",
    borderRadius: 10,
    marginHorizontal: 15,
    paddingHorizontal: 5,
    paddingVertical: 5,
    flex: 0
  }
})

export { ModalMeal }
