import React, { useState, useEffect } from "react"
import { Button } from "react-native-elements"
import { Text, View, StyleSheet, Picker } from "react-native"
import { logOut, changeUserRole } from "../actions/authActions"
import { setLanguage } from "../actions/settingsActions"
import { connect } from "react-redux"
import LANG from "../constants/Lang"
// This component is rendered when the user IS logged in
const UserLoggedIn = props => {
  const [role, setRole] = useState(props.auth.user.roleId)
  const onLogout = async () => {
    props.logOut()
  }

  // Function that is called when props.auth.user.roleId changes in redux
  useEffect(() => {
    setRole(props.auth.user.roleId)
  }, [props.auth.user.roleId])

  // When user wants to authenticate as a student or staff, this is the place to do it
  const authWithShibboleth = async itemValue => {
    const msg = {
      2: "Tunnistautuminen opiskelijaksi onnistui!",
      3: "Tunnistautuminen henkilökunnaksi onnistui!"
    }

    // Login with shibboleth and if successful, we can inform our backend and update db:
    const authSuccesful = true // We are setting this true, since no real authentication is done at this point

    if (authSuccesful) {
      console.log("ITEMVALUE", itemValue)
      props.changeUserRole(props.auth.user, itemValue, msg[itemValue])
    }
  }

  return (
    <View style={styles.containerStyle}>
      <Text style={styles.nameStyle}>
        {LANG.WELCOME[props.lang]} {props.auth.user.email}!
      </Text>
      <View style={styles.bottomStyle}>
        <Text>{LANG.AUTHENTICATE_AS[props.lang]}</Text>
        <Picker
          selectedValue={role}
          style={styles.pickerStyle}
          onValueChange={itemValue => authWithShibboleth(itemValue)}
        >
          <Picker.Item label='Vieras' value={1} />
          <Picker.Item label='Opiskelija' value={2} />
          <Picker.Item label='Henkilökunta' value={3} />
        </Picker>
      </View>
      <View style={styles.bottomStyle}>
        <Text>{LANG.CHOOSE_LANGUAGE[props.lang]}</Text>
        <Picker
          selectedValue={props.lang}
          style={styles.pickerStyle}
          onValueChange={itemValue => props.setLanguage(itemValue)}
        >
          <Picker.Item label='Suomi' value={"fi"} />
          <Picker.Item label='English' value={"en"} />
        </Picker>
      </View>
      <Button
        buttonStyle={styles.buttonStyle}
        onPress={() => onLogout()}
        title={LANG.LOGOUT[props.lang]}
      />

      {/* <Text>{JSON.stringify(props.auth, null, 2)}</Text> */}
    </View>
  )
}

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#019dd5"
  },
  containerStyle: {
    marginTop: 20,
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center"
  },
  nameStyle: {
    fontWeight: "bold",
    fontSize: 17
  },
  pickerStyle: {
    width: 250,
    height: 50
  },
  bottomStyle: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(242, 135, 183, 0.3)"
  }
})

const mapStateToProps = state => ({
  auth: state.auth,
  lang: state.settings.lang
})

export default connect(mapStateToProps, {
  logOut,
  changeUserRole,
  setLanguage
})(UserLoggedIn)
