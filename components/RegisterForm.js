import React, { useState } from "react"
import { Button, Input } from "react-native-elements"
import { Text, View, StyleSheet, Image } from "react-native"
import { registerNewUser, loginUser } from "../actions/authActions"
import { connect } from "react-redux"
import LANG from "../constants/Lang"
// This component will be rendered in case the user is NOT logged in

const RegisterForm = props => {
  const [form, onFormChange] = useState({ email: "", password: "" })
  console.log(form)

  // This will call the action defined in ../actions/authActions when user clicks register
  const onRegister = async () => {
    props.registerNewUser(form.email, form.password)
  }
  // Same for login..
  const onLogin = async () => {
    props.loginUser(form.email, form.password)
  }

  return (
    <View style={styles.containerStyle}>
      <Image
        style={{ marginBottom: 20 }}
        source={require("../assets/images/barLaurea_small.png")}
      />

      <Input
        inputStyle={styles.inputStyle}
        containerStyle={styles.inputFieldContainerStyle}
        inputContainerStyle={styles.inputContainerStyle}
        placeholder={LANG.PLACEHOLDER_EMAIL[props.lang]}
        onChangeText={text =>
          onFormChange({
            ...form,
            email: text
          })
        }
      />
      <Input
        inputStyle={styles.inputStyle}
        containerStyle={styles.inputFieldContainerStyle}
        inputContainerStyle={styles.inputContainerStyle}
        textContentType='password'
        secureTextEntry={true}
        placeholder={LANG.PLACEHOLDER_PASSWORD[props.lang]}
        onChangeText={text =>
          onFormChange({
            ...form,
            password: text
          })
        }
      />
      <Button
        buttonStyle={styles.buttonStyle}
        containerStyle={styles.buttonContainerStyle}
        onPress={() => onLogin()}
        title={LANG.TITLE_LOGIN[props.lang]}
      />
      <Text style={{ marginVertical: 5 }}>{LANG.OR[props.lang]}</Text>
      <Button
        buttonStyle={styles.buttonStyle}
        containerStyle={styles.buttonContainerStyle}
        onPress={() => onRegister()}
        title={LANG.TITLE_REGISTER[props.lang]}
      />

      <Text style={{ marginTop: 20, textAlign: "center" }}>
        {LANG.AGREEMENT[props.lang]}
        <Text style={styles.linkStyle}>
          {LANG.TERMS_OF_USE[props.lang]}
        </Text>{" "}
        {LANG.AND[props.lang]}
        <Text style={styles.linkStyle}>{LANG.PRIVACY_POLICY[props.lang]}</Text>
      </Text>

      {/* <Text>{JSON.stringify(props.auth, null, 2)}</Text> */}
    </View>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    marginTop: 20,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 20
  },
  inputStyle: {
    textAlign: "center"
  },
  inputContainerStyle: {
    borderBottomColor: "rgba(242, 135, 183, 0.5)"
  },
  inputFieldContainerStyle: {
    maxWidth: "90%",
    marginBottom: 20
  },
  buttonStyle: {
    backgroundColor: "#019dd5"
  },
  buttonContainerStyle: {
    width: "50%",
    maxWidth: 200
  },
  linkStyle: {
    color: "#019dd5",
    textDecorationLine: "underline",
    textDecorationColor: "#019dd5"
  }
})

const mapStateToProps = state => ({
  auth: state.auth,
  lang: state.settings.lang
})

export default connect(mapStateToProps, { registerNewUser, loginUser })(
  RegisterForm
)
