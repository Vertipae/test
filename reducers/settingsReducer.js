import { SET_LANGUAGE } from "../actions/types"
// This is the initial state for lang state when the application starts
// If you want to set english to be the default lang, use 'en'
const initialState = {
  lang: "fi"
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_LANGUAGE:
      console.log("SET_LANGUAGE PAYLOAD", action.payload)
      return {
        ...state,
        lang: action.payload
      }
    default:
      return state
  }
}
