import { combineReducers } from "redux"
import authReducer from "./authReducer"
import ticketReducer from "./ticketReducer"
import settingsReducer from "./settingsReducer"

// This combines all the reducers
const rootReducer = combineReducers({
  auth: authReducer,
  tickets: ticketReducer,
  settings: settingsReducer
})

export default rootReducer
