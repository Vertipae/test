import { ADD_TICKET, DELETE_TICKET, SET_TICKETS } from "../actions/types"

// This is the initial state for tickets when the application starts
const initialState = {
  tickets: []
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_TICKETS:
      // Override old tickets
      return {
        ...state,
        tickets: action.payload
      }

    case ADD_TICKET:
      return {
        ...state,
        tickets: [action.payload, ...state.tickets]
      }
    default:
      return state
  }
}
