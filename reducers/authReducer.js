import { SET_CURRENT_USER } from "../actions/types"
import { CLEAR_USER } from "../actions/types"
import isEmpty from "../validation/is-empty"

// This is the initial state for user auth state when the application starts
const initialState = {
  isAuthenticated: false,
  user: {}
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      console.log("PAYLOAD", action.payload)
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      }
    case CLEAR_USER:
      return {
        ...state,
        isAuthenticated: false,
        user: {}
      }
    default:
      return state
  }
}
