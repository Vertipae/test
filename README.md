## Mobile dev environment

1. Clone the repository https://gitlab.com/Vertipae/test.git
2. In project root, run npm install to install the dependencies
3. Run npm start
4. Type 'y' for installing Expo CLI (a prompt will appear in case it is not installed previously)

Now the development environment is running. The application can be started by using emulators or in a physical device by using Expo application. We used real devices instead of emulators, to use a real device with Expo application, do the following:

1. Go to Play Store and install Expo (Expo project)
2. Launch the application and scan the QR-code in your terminal window, the first build will take some time
3. The application will launch automatically after the build is finished

Note that for this to work, the phone and development environment needs to be running in the same network, so make sure your phone is connected to WiFi instead of 4g mobile network. It is also possible to access it from outside of the local network, in this case please refer to Expo documentation. https://docs.expo.io/versions/latest/
